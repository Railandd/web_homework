import './App.css';
import React from 'react';
import CookiesManager from 'js-cookie';
import LoginModal from './components/LoginModal'
import Header from './components/Header';
import Navigator from './components/Navigator'

class App extends React.Component {
  state = {
    isLoggedIn: false,
    fio: "Андреев Руслан Игоревич",
    token: undefined
  }

  hadleLoginClick = () => {
    if (this.state.isLoggedIn === false) {
      this.setState({ isLoggedIn: true })
      CookiesManager.set('isLoggedIn', true);
    }
    else {
      CookiesManager.set('isLoggedIn', false);
      CookiesManager.set('token', undefined);
      this.setState({ isLoggedIn: false })
    }
  }

  setToken = (s_token) => {
    this.setState({ token: s_token })
    CookiesManager.set('token', s_token);
  }

  render() {
    return <div className='app'>
      {this.state.isLoggedIn === true ? <div className='mainScreen'>
        <Header onButtonClick={this.hadleLoginClick} fio={this.state.fio} />
        <Navigator />
      </div> : <LoginModal onButtonClick={this.hadleLoginClick} isLoggedIn={this.state.isLoggedIn} setToken={this.setToken} token={this.state.token} />}
    </div>;
  }
}

export default App;
