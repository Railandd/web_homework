import React from 'react';
import CookiesManager from 'js-cookie';

const GET_TOKEN_URL = "http://ruslanandreev.pythonanywhere.com/api-token-auth/"

class LoginModal extends React.Component {
    constructor(props) {
        super(props);
        this.loginRef = null;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    state = {
        failedLogin: false
    }

    handleChange(event) {
        this.setState({value: event.target.value});
      }
    
    handleSubmit(event) {
        this.tryLogin();
        event.preventDefault();
    }

    tryLogin = async () => {
        const { onButtonClick } = this.props;
        const { setToken } = this.props;
        let username = this.loginRef.value;
        let password = this.passwordRef.value;
        try {
            const result = await fetch(GET_TOKEN_URL, {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    'username': username,
                    'password': password
                })
            })
            let token_tmp = await result.json();
            if ('token' in token_tmp) {
                setToken(token_tmp.token);
                onButtonClick();
            }
            else {
                this.setState({ failedLogin: true });
            }

        } catch (err) {
            this.setState({
                error: "Ошибка получения данных"
            })
        }
    }

    componentDidMount = async () => {
        let cookie_loginned = CookiesManager.get('isLoggedIn');
        let cookie_token = CookiesManager.get('token');
        if (cookie_loginned === "true" && cookie_token) {
            const { onButtonClick } = this.props;
            const { setToken } = this.props;
            setToken(cookie_token);
            onButtonClick();
        }
    }

    render() {
        let { failedLogin } = this.state;
        return (
            <form onSubmit={this.handleSubmit} className="loginWindow">
                <h1>Вход</h1>
                <div>
                    <label>Логин<input className='formRight' type="text" name="username" size="20" maxLength="50" ref={ref => this.loginRef = ref} /></label>
                </div>
                <div>
                    <label>Пароль <input type="password" name="password" size="20" maxLength="50" ref={ref => this.passwordRef = ref} /></label>
                </div>
                <input type="submit" value="Войти"  className="enterButton"/>
                {failedLogin ? <div className='fail'>Неверный логин или пароль</div> : <div></div>}
            </form>
        )
    }
}

export default LoginModal;