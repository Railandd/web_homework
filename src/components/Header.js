import React from 'react';

class Header extends React.Component {
    handleClick = () => {
        const { onButtonClick } = this.props;
        onButtonClick();
    }

  render() {
    return <div className='header'>
        <div className='fio'> {this.props.fio} </div>
        <button onClick={this.handleClick} className='escapeButton' >Выйти</button>
    </div>;
    }
}

export default Header;